$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    $("#contacto").on('show.bs.modal', function (e){
        console.log("El modal se esta mostrando");
        $('#contactoBtn').removeClass('btn-outline-success');
        $('contactoBtn').addClass('btn-primary');
    });
    $("#contacto").on('shown.bs.modal', function (e){
        console.log("El modal se mostro");
    });
    $("#contacto").on('hide.bs.modal', function (e){
        console.log("El modal se oculta");
    });
    $("#contacto").on('hidden.bs.modal', function (e){
        console.log("El modal se oculto");

    });
});