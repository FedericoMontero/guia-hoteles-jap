module.exports = function(grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass:{
            dist:{
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest:'css',
                    ext:'.css'
            
                }]
            }
        },

        copy:{
            html:{
                files:[{
                    expand:true,
                    dot:true,
                    cwd:'./',
                    src:['*.html'],
                    dest:'dist'
                }]
            },
        },

        clean:{
            build:{
                src:['dist/']
            }
        },

        

        watch:{
            files:['css/*.scss'],
            tasks: ['css']
        },

        browserSync:{
            dev:{
                bsFiles:{//browser files
                    src:[
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options:{
                    watchTask:true,
                    server:{
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },

        imagemin:{
            dynamic:{
                files:[{
                    expand:true,
                    cwd:'./',
                    src:'img/*.{png,gif,jpg,jpeg}',
                    dest:'dist/'
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('css',['sass']);
    grunt.loadNpmTasks('default', ['browserSync','watch']);
    grunt.loadNpmTasks('img:compress',['imagemin']);
};